# ppp

Forked version of ppp with ClearOS changes applied

* git clone git@gitlab.com:clearos/upstream/centos/ppp.git
* cd ppp
* git checkout c7
* git remote add upstream https://git.centos.org/rpms/ppp.git
* git pull upstream c7
* git checkout clear7
* git merge --no-commit c7
* git commit
